# CHANGELOG

## v1.0.0
* add planets
* add spaceships

## v1.0.5
* add droids

## v1.0.6
* remove c wing
